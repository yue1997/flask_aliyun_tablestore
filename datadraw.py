#  coding: utf-8
import base64
import json
import numpy as np
import matplotlib.pyplot as plt
from io import BytesIO
from table import TableSql


class DataDraw:
    i = []

    def __init__(self, data=None):
        if data is None:
            print('无数据')
        else:
            for row in data:
                temarray = json.loads(row[1])
                temnp = np.array(temarray)
                temnp.reshape(370, 350)
                a = np.max(temnp)
                self.i.append(a)
                self.x = range(len(self.i))

    def drawline(self, x=None):
        plt.figure()
        if x is None:
            plt.plot(self.x, self.i, linewidth=1)
        else:
            plt.plot(x, self.i, linewidth=1)
        plt.plot(self.i, 'ro')
        plt.xlabel('times')
        plt.ylabel('tem')
        plt.title('line plot')
        s = BytesIO()
        plt.savefig(s)
        im = base64.b64encode(s.getvalue()).decode()
        return im
        # print(s.getvalue())
        # print(base64.b64encode(s.getvalue()))


if __name__ == '__main__':
    abc = TableSql()
    abd = DataDraw(abc.getrange())
    abd.drawline()
