# coding: utf-8
import base64
import json
import cv2
import numpy as np
from io import BytesIO
from skimage import draw
from PIL import Image
from table import TableSql


class DataProcess:
    alert = None  # 警报
    yuan = None  # 原图，base64格式
    img = None  # 原图画布，numpy矩阵
    tem = None  # 当前温度矩阵
    tem1 = None  # 之前温度矩阵

    def __init__(self, data=None):
        if data is None:
            print("data can't be None")
        # 获取图片信息并转换为需求的类型
        else:
            s = BytesIO()
            m = base64.b64decode(data[0][1][2:].strip('\''))
            # print(type(m))  # bytes
            s.write(m)
            # print(type(s.getvalue()))  # bytes
            img = Image.open(s)
            self.yuan = data[0][1][2:].strip('\'')
            self.img = np.array(img)
            # 获取数组信息并转换为需求的矩阵
            temarray = json.loads(data[1][1])
            temnp = np.array(temarray)
            temnp.reshape(370, 350)
            self.tem = temnp
            if data[2][1] is not None:
                temarray1 = json.loads(data[2][1])
                temnp1 = np.array(temarray1)
                temnp1.reshape(370, 350)
                self.tem1 = temnp1  # 之前的矩阵
            # self.maxtem()
            # self.mixcha()

    def maxtem(self, a=99):
        b = np.max(self.tem)
        d = np.argwhere(self.tem == b)
        if d is not None:
            for i in d:
                rr, cc = draw.circle(i[0], i[1], 5)
                draw.set_color(self.img, [rr, cc], [0, 0, 0])
            if b >= a:
                self.alert = '超温警报'
        else:
            self.alert = '无'
        return d

    def mixcha(self, a=99):
        img = cv2.absdiff(self.tem1, self.tem)
        d = np.argwhere(img >= a).tolist()  # type: np.ndarray
        if d is not None:
            if self.alert == '超温警报':
                self.alert = '双重警报'
            elif self.alert == '无':
                self.alert = '温差过大'
            # for i in d:
            #     rr, cc = draw.circle(i[0], i[1], 5)
            #     # print([rr, cc])
            #     draw.set_color(self.img, [rr, cc], [0, 0, 255])
        else:
            self.alert = '无'
        return d


if __name__ == '__main__':
    abc = TableSql()
    abcd = DataProcess(abc.get_nowrow())

    # plt.imshow(abcd.img)
    # plt.show()
