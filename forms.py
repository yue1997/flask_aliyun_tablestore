from flask_wtf import FlaskForm
from wtforms import DateField, TimeField, SubmitField
from wtforms.validators import DataRequired


class DateForm(FlaskForm):
    date = DateField('日期', validators=[DataRequired(message='请选择日期')], format='%Y-%m-%d')
    time = TimeField('时间', validators=[DataRequired(message='请选择时间')], format='%H:%M')
    submit = SubmitField('查询')
