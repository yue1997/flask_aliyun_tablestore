# coding: utf-8
import cv2
import json
import numpy

a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
j = json.dumps(a)
print(type(j))
print(j)
b = "[[1, 2, 3], [4, 5, 6], [7, 8, 9]]"
j1 = json.loads(j)
print(type(j1))
print(j1)

#  检索特定值的位置
a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
c = [[3, 1, 2], [5, 6, 4], [8, 7, 9]]
b = numpy.matrix(a)
e = numpy.matrix(c)
#  两个numpy矩阵相差的绝对值
img = cv2.absdiff(b, e)
print(img)
d = numpy.argwhere(b >= 7).tolist()
print("d中为值的位置:%s", d)
