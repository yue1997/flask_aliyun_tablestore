# coding: utf-8
import json


a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
jsona = json.dumps(a, ensure_ascii=False)  # 将 Python 对象编码成 JSON 字符串
print(type(jsona))
b = json.loads(jsona)  # 将已编码的 JSON 字符串解码为 Python 对象
print(type(b))

# with open('json_array.json', 'w+') as j:
#     j.write(jsona)
#     j.close()

# with open('json_array.json', 'r') as f:
#     b = json.loads(f.read())
#     print(b)
#     f.close()

# with open('moni/wendushuzu', 'r') as w:
#     n = json.loads(w.read())
#     print(n[0])
#     w.close()
