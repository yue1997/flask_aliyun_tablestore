
# coding: utf-8
import json

from PIL import Image
import numpy as np
import matplotlib.pyplot as mat
import scipy.misc
#  读取图片


im = Image.open("road.jpg")
w, h = im.size
print(w, h)
#  显示图片
# mat.imshow(im)
# mat.show()
#  将图片以数组的形式读入变量
image_arr = np.array(im)
print(type(image_arr))
#  将数组转换为图片并显示出来
newim = scipy.misc.toimage(image_arr)
mat.imshow(newim)
mat.show(newim)

# data = mat.imread(im)  报错代码
# data = np.reshape(data, (w, h))
# new_in = Image.fromarray(data)
# print(data)

#  图像转json测试 成功
# yuantu = Image.open('moni/yuantu.jpg')
# hongwaitu = Image.open('moni/hongwaitu.jpg')
# yuantu_array = np.array(yuantu)
# hongwaitu_array = np.array(hongwaitu)
# with open('moni/yuantu.json', 'w+') as y:
#     y.write(json.dumps(yuantu_array.tolist()))
#     y.close()
# with open('moni/hongwaitu.json', 'w+') as h:
#     h.write(json.dumps(hongwaitu_array.tolist()))
#     h.close()

#  读取json画图测试 成功
# with open('moni/hongwaitu.json','r+') as h:
#     hongwaitu_array = json.loads(h.read())
#     hongwaitu = scipy.misc.toimage(hongwaitu_array)
#     hongwaitu.show()
#     h.close()


#  生成一个随机温度数组 保持精度为一位小数
# tem_array = 100*np.random.random_sample((214, 181))
# print(np.round(tem_array, 1))
