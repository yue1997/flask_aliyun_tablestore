import base64
import random

import sys
from PIL import Image
from skimage import draw
import numpy as np
import matplotlib.pyplot as plt
import json

#  读取数组在图上标识特定点
im = open('moni/hongwaitu.json').read()
im_array = np.array(json.loads(im))
print(type(im_array))
#  随机生成的X/Y轴坐标
tem_arrayy = np.round(181*np.random.random_sample((3)),).tolist()  # Y轴坐标
tem_arrayx = np.round(214*np.random.random_sample((3)),).tolist()  # X轴坐标

rr, cc = draw.circle(int(tem_arrayx[0])+5, int(tem_arrayy[0])+5, 5)
print(rr, cc)
draw.set_color(im_array, [rr, cc], [0, 0, 255])  # 在im_array上标识特殊点

plt.imshow(im_array)
plt.show()


#  随机生成的温度数组
# with open('moni/wendushuzu.txt', 'w+') as w:
#     tem_array = np.round(100 * np.random.random_sample((370, 350)), 1).tolist()
#     tem = json.dumps(tem_array)
#     print(sys.getsizeof(tem))
#     # w.write(tem)
#     w.close()
# tem_array = np.round(100 * np.random.random_sample((370, 350)), 1)
# # tem_array1 = np.random.randint(100, size=(370, 350))
#
#
# s = base64.b64encode(bytes(tem_array))
# print(type(s))
# with open('moni/wendushuzu.txt', 'wb') as w1:
#     w1.write(s)
#     w1.close()


# print(sys.getsizeof(random_int_list(0, 100, 350*370)))
# print(sys.getsizeof(tem_array))
# print(sys.getsizeof(tem_array1))
