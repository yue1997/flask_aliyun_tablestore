# coding: utf-8
import base64
from PIL import Image
from flask import Flask, render_template
from flask_moment import Moment
from flask_bootstrap import Bootstrap
from dataprocess import DataProcess
from datadraw import DataDraw
from table import TableSql
from io import BytesIO
from datetime import datetime
import time
from flask_nav import Nav
from flask_nav.elements import *

app = Flask(__name__)
Bootstrap(app)
nav = Nav()

# 定义导航栏
nav.register_element('top', Navbar(u'温度信息', View(u'主页', 'index'),
                                   # Subgroup(u'选择设备', View(u'设备一', 'index'),
                                   #          View(u'设备二', 'index')),
                                   View(u'查询历史温度', 'gethistory'),
                                   View(u'查询历史曲线', 'base')))
nav.init_app(app)  # 注册到app上
moment = Moment(app)  # 显示时间插件
app.config['SECRET_KEY'] = 'asdf123qwer456'  # 访问秘钥

abc = TableSql()  # 提前实例连接对象


@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html', current_time=datetime.utcnow())


@app.route('/img/')
def img():
    return
    # abcd = DataProcess(abc.get_nowrow())
    # img = abcd.yuan
    # img_1 = Image.fromarray(abcd.img)
    # output_buffer = BytesIO()
    # img_1.save(output_buffer, format='png')
    # byte_data = output_buffer.getvalue()
    # img1 = base64.b64encode(byte_data).decode()
    # return render_template('hello.html', img=img, img1=img1)


@app.route('/postmsg', methods=['post'])
def postmsg():
    id1 = request.form.get('robot')
    if request.form.get('date') is not None:
        msg = str(request.form.get('date')) + ' ' + request.form.get('hour') + ":" + request.form.get('minute')
        if request.form.get('date1') is not None:
            msg1 = str(request.form.get('date1')) + ' ' + request.form.get('hour1') + ":" + request.form.get(
                'minute1')
            timest = int(time.mktime(time.strptime(msg, "%Y-%m-%d %H:%M")))
            timest1 = int(time.mktime(time.strptime(msg1, "%Y-%m-%d %H:%M")))
            try:
                abd = DataDraw(abc.getrange(timest, timest1))
            except AttributeError:
                return "该时间段暂无数据"
            im = abd.drawline()
            return render_template('tem_historyline.html', current_time=datetime.utcnow(),
                                   im=im,
                                   msg=msg,
                                   msg1=msg1)
        # return str(time.mktime(time.strptime(msg, "%Y-%m-%d %H:%M"))) #这时间戳为float
        try:
            abcd = DataProcess(abc.get_nowrow(a=str(id1), c=int(time.mktime(time.strptime(msg, "%Y-%m-%d %H:%M")))))
        except AttributeError:
            return id1 + "暂无数据或" + msg + "该时间暂无数据"
        img = abcd.yuan
        img_1 = Image.fromarray(abcd.img)
        output_buffer = BytesIO()
        img_1.save(output_buffer, format='png')
        byte_data = output_buffer.getvalue()
        img1 = base64.b64encode(byte_data).decode()
        jingbao = abcd.alert
        return render_template('tem_history.html', current_time=datetime.utcnow(),
                               img=img,
                               img1=img1,
                               msg=msg,
                               jingbao=jingbao)
    elif request.form.get('date') is None:
        try:
            abcd = DataProcess(abc.get_nowrow(a=str(id1)))
        except AttributeError:
            return id1 + "暂无数据"
        img = abcd.yuan
        img_1 = Image.fromarray(abcd.img)
        output_buffer = BytesIO()
        img_1.save(output_buffer, format='png')
        byte_data = output_buffer.getvalue()
        img1 = base64.b64encode(byte_data).decode()
        timest = '2018-06-15 18:10:00'
        jingbao = abcd.alert
        return render_template('tem_now.html', current_time=datetime.utcnow(),
                               img=img,
                               img1=img1,
                               timest=timest,
                               jingbao=jingbao)


@app.route('/base')
def base():
    return render_template('base.html', current_time=datetime.utcnow())


@app.route('/gethistory')
def gethistory():
    return render_template('gethistory.html', current_time=datetime.utcnow())


if __name__ == '__main__':
    app.run(debug=True)
