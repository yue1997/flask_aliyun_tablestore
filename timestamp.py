
# coding: utf-8
import datetime
import time

#  获取当前时间戳
y = int(time.time())
print(y)
#  获取当前时间
now_time = datetime.datetime.now()
timearray = now_time.strftime("%Y-%m-%d %H:%M:%S")
print(timearray)
#  当前时间转时间戳
timestamp = time.mktime(time.strptime(timearray, "%Y-%m-%d %H:%M:%S"))
print(timestamp)
#  时间戳转时间
timeArray = time.localtime(timestamp)
print(timeArray)
otherStyleTime = time.strftime("%Y--%m--%d %H:%M:%S", timeArray)
print(otherStyleTime)

# datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
