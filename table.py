import base64
import json
from io import BytesIO
from tablestore.client import OTSClient, OTSServiceError
from tablestore.error import OTSClientError
from tablestore import Row, Condition
from PIL import Image
import numpy as np


class TableSql:
    # a = 'https://test2018.cn-hangzhou.ots.aliyuncs.com'
    # b = 'LTAIIOhDEc1eUPxg'
    # c = 'e5mTC52LcE0cfuciokx7VJUZqOaVYH'
    # d = 'test2018'
    # client = OTSClient(a, b, c, d)

    def __init__(self, a='https://test2018.cn-hangzhou.ots.aliyuncs.com',
                 b='LTAIIOhDEc1eUPxg',
                 c='e5mTC52LcE0cfuciokx7VJUZqOaVYH',
                 d='test2018'):
        self.client = OTSClient(a, b, c, d)
    # list_response = client.list_table()
    # print ('instance table:')
    # for table_name in list_response:
    #     print (table_name)
    # primary_id = [('ID', 2)]

    def get_nowrow(self, a='robot1', b="theTem", c=1529057400):
        primary_id = [('robotid', a), ('timestamp', c)]
        consumed, return_row, next_token = self.client.get_row(b, primary_id)
        primary_id1 = [('robotid', a), ('timestamp', c-600)]
        columns_to_get = ['temarray']
        consumed1, return_row1, next_token1 = self.client.get_row(b, primary_id1, columns_to_get)
        #  表示消耗的CapacityUnit，是tablestore.metadata.CapacityUnit类的实例
        # print('Read succeed, consume %s read cu.' % consumed.read)
        # print('Value of primary key: %s' % return_row.primary_key)#主键
        # print('Value of attribute: %s' % return_row.attribute_columns)#列值
        # print(type(return_row.attribute_columns))
        a = return_row.attribute_columns + return_row1.attribute_columns
        # for att in a:
        #     # 打印出每一列的key，value和version值。
        #     print('name:%s\tvalue:%s\ttimestamp:%d' % (att[0], att[1], att[2]))
        # print(a)
        return a

    def getrange(self, a=1529056200, b=1529057400):
        start_primary_key = [('robotid', "robot1"), ('timestamp', a)]
        end_primary_key = [('robotid', "robot1"), ('timestamp', b+1)]
        columns_to_get = ['temarray']
        try:
            # 调用get_range接口
            consumed, next_start_primary_key, row_list, next_token = self.client.get_range(
                "theTem", 'FORWARD',
                start_primary_key, end_primary_key,
                columns_to_get
            )
            all_rows = []
            list_rows = []
            all_rows.extend(row_list)
            for row in all_rows:
                list_rows.extend(row.attribute_columns)
                # print(row.primary_key, row.attribute_columns)
            # print('Total rows: ', len(all_rows))
            # print(list_rows[0][1])
            return list_rows
        # 客户端异常，一般为参数错误或者网络异常。
        except OTSClientError as e:
            print("get row failed, http_status:%d, error_message:%s" % (e.get_http_status(), e.get_error_message()))

        # 服务端异常，一般为参数错误或者流控错误。
        except OTSServiceError as e:
            print("get row failed, http_status:%d, error_code:%s, error_message:%s, request_id:%s" % (
                 e.get_http_status(), e.get_error_code(), e.get_error_message(), e.get_request_id()))

    #  写入数据 ，插入行
    def put_arow(self, a='robot1', b=1529042400, c=1):
        primary_key = [('robotid', a), ('timestamp', b)]
        path1 = 'randomimg/test' + str(c) + '.txt'
        path2 = 'randomimg/testtem' + str(c) + '.txt'
        img = open(path1, 'r').read()
        # tem_array2 = np.random.randint(0, 100, (370, 350))
        # tem_list2 = tem_array2.tolist()
        # temjson = json.dumps(tem_list2)
        tem = open(path2, 'rb').read()
        attribute_columns = [('img', img), ('temarray', tem)]
        row = Row(primary_key, attribute_columns)
        condition = Condition('EXPECT_NOT_EXIST')
        try:
            # 调用put_row方法，如果没有指定ReturnType，则return_row为None。
            consumed, return_row = self.client.put_row('theTem', row, condition)
            # 打印出此次请求消耗的写CU。
            print('put row succeed, consume %s write cu.' % consumed.write)
            # 客户端异常，一般为参数错误或者网络异常。
        except OTSClientError as e:
            print("put row failed, http_status:%d, error_message:%s" % (e.get_http_status(), e.get_error_message()))
            # 服务端异常，一般为参数错误或者流控错误。)
        except OTSServiceError as e:
            print("put row failed, http_status:%d, error_code:%s, error_message:%s, request_id:%s" % (
                e.get_http_status(), e.get_error_code(), e.get_error_message(), e.get_request_id()))


if __name__ == '__main__':
    abc = TableSql()
    # abc.get_nowrow()
    abc.getrange()
    # print(data[1][2])
    # print(type(data[1][2]))
    # abc.getrange() #  测试范围查询
    # abc.put_arow()

    # for i in range(1, 27):
    #     ti = 1529042400 + 600*(i-1)
    #     abc.put_arow(b=ti, c=i)
    # Ddata = abc.get_nowrow(c=1529042400)
    #  从数据库读取图片信息转换为PIL中img对象
    # s = BytesIO()
    # m = base64.b64decode(Ddata[0][1][2:])
    # s.write(m)
    # img = Image.open(s)
    # print(type(img))
    # s.close()
    # print(type(Ddata[0][1][2:]))

    #  将图片信息提取出来存入本地
    # with open('test.jpg', 'wb') as t:
    #     t.write(base64.b64decode(Ddata[0][1][2:]))
    #     t.close()

    # print(Ddata[1][1])
    # with open('temlist.txt', 'w+') as t:
    #     t.write(Ddata[1][1])
    #     t.close()

    #  从数据库读取数组信息转换为可操作的numpy矩阵
    # temarray = json.loads(Ddata[1][1])
    # print(type(temarray))
    # temnp = np.array(temarray)
    # temnp.reshape(370,350)
    # print(temnp.shape)

    # print(type(Ddata))
    # print(Ddata[0][1])
    # print(type(Ddata[0][1]))
    # st1 = Ddata[0][1][2:]
    # print(st1)
#         try:
#             # consumed, return_row, next_token = client.get_row('robot1', primary_id)
#             # print('Read succeed, consume %s read cu.' % consumed.read)
#             # print('Value of primary key: %s' % return_row.primary_key)
#             # print('Value of attribute: %s' % return_row.attribute_columns)
#             # for att in return_row.attribute_columns:
#             #     # 打印出每一列的key，value和version值。
#             #     print('name:%s\tvalue:%s\ttimestamp:%d' % (att[0], att[1], att[2]))
#             abc = TableSql
#             abc.get_onerow(abc)
#         except OTSClientError as e:
#             print("get row failed, http_status:%d, error_message:%s" % (e.get_http_status(), e.get_error_message()))
